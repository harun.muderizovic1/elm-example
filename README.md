Copy of the examples on [GitHub](https://github.com/hrnxm/elm-architecture/) with added CSS, and _.gitlab-ci.yml_ to deploy the app.

[Yarn](https://classic.yarnpkg.com/en/docs/install/) package manager is recommended

    yarn install
    yarn make # to (re)compile elm code

    # for a clean build
    yarn clean
    yarn install
    yarn make

You can preview `index.html` in your browser to see the result

GitLab YML uses basic Node docker image with Node and Yarn.
It contains _pages_ stage with a job of the same name.
Pipeline is run whenever you push to any branch.
Job compiles Elm and generates static files which are moved to _public_ dir (as a convention).
GitLab pages serve files from that directory, so you can preview it [here](https://elm-example-harun-muderizovic1-6d045a838f0c5b0e785c68b9f8e1f76f.gitlab.io/).

You can add this file to your project, probably update _pages>script_ part, and preview your front-end app. The link for your app will be available at:

`https://gitlab.com/*username*/*project*/pages`

Note: for pages to work, your project needs to be public.
